

CREATE SCHEMA IF NOT EXISTS `BOOKING_RESTAURANT` DEFAULT CHARACTER SET utf8 ;
USE `BOOKING_RESTAURANT` ;

-- -----------------------------------------------------
-- Table `BOOKING_RESTAURANT`.`RESTAURANT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOOKING_RESTAURANT`.`RESTAURANT` (
  `ID` INT(19) NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(45) NULL,
  `DESCRIPTION` VARCHAR(100) NULL,
  `ADDRESS` VARCHAR(100) NULL,
  `IMAGE` VARCHAR(500) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BOOKING_RESTAURANT`.`RESERVATION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOOKING_RESTAURANT`.`RESERVATION` (
  `ID` INT(19) NOT NULL AUTO_INCREMENT,
  `LOCATOR` VARCHAR(45) NULL,
  `PERSON` INT(19) NULL,
  `DATE` DATE NULL,
  `TURN` VARCHAR(45) NULL,
  `RESTAURANTE_ID` INT(19) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_RESERVATION_RESTAURANTE_idx` (`RESTAURANTE_ID` ASC),
  CONSTRAINT `fk_RESERVATION_RESTAURANTE`
    FOREIGN KEY (`RESTAURANTE_ID`)
    REFERENCES `BOOKING_RESTAURANT`.`RESTAURANT` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BOOKING_RESTAURANT`.`TURN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOOKING_RESTAURANT`.`TURN` (
  `ID` INT(19) NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(45) NULL,
  `RESTAURANT_ID` INT(19) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_TURN_RESTAURANT1_idx` (`RESTAURANT_ID` ASC) ,
  CONSTRAINT `fk_TURN_RESTAURANT1`
    FOREIGN KEY (`RESTAURANT_ID`)
    REFERENCES `BOOKING_RESTAURANT`.`RESTAURANT` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BOOKING_RESTAURANT`.`BOARD`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOOKING_RESTAURANT`.`BOARD` (
  `ID` INT(19) NOT NULL AUTO_INCREMENT,
  `CAPACITY` INT(19) NULL,
  `NUMBER` INT(19) NULL,
  `RESTAURANT_ID` INT(19) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_BOARD_RESTAURANT1_idx` (`RESTAURANT_ID` ASC) ,
  CONSTRAINT `fk_BOARD_RESTAURANT1`
    FOREIGN KEY (`RESTAURANT_ID`)
    REFERENCES `BOOKING_RESTAURANT`.`RESTAURANT` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/************* INSERTS ************/
INSERT INTO `booking_restaurant`.`restaurant` (`ID`, `NAME`, `DESCRIPTION`, `ADDRESS`, `IMAGE`) VALUES ('1', 'Burger', 'Todo tipo de hamburgesas', 'Calle Puig 4', 'https://financierolatam.com/wp-content/uploads/2020/07/PorfiriosPor.jpg');
INSERT INTO `booking_restaurant`.`restaurant` ( `NAME`, `DESCRIPTION`, `ADDRESS`, `IMAGE`) 
VALUES ( 'Chicken', 'Pollo frito ', 'Calle Ladud #23', 'https://cdn.recetas360.com/wp-content/uploads/2020/03/como-hacer-el-pollo-kfc.jpeg');



INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_10_00', '1');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_11_00', '1');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_12_00', '1');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_13_00', '1');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_10_00', '2');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_11_00', '2');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_12_00', '2');
INSERT INTO `booking_restaurant`.`turn` (`NAME`, `RESTAURANT_ID`) VALUES ('TURNO_13_00', '2');






INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('3', '1', '1');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('6', '2', '1');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('2', '3', '1');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('5', '4', '1');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('4', '1', '2');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('6', '2', '2');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('4', '3', '2');
INSERT INTO `booking_restaurant`.`board` (`CAPACITY`, `NUMBER`, `RESTAURANT_ID`) VALUES ('8', '4', '2');


INSERT INTO `booking_restaurant`.`reservation` (`LOCATOR`, `PERSON`, `DATE`, `TURN`, `RESTAURANTE_ID`) VALUES ('Burger ', '6', '2021-11-27', 'TURNO_10_00', '1');










