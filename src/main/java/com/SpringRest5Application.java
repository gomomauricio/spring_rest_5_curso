package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.atributo.Coche;
import com.atributo.Motor;

@SpringBootApplication
public class SpringRest5Application {

	public static void main(String[] args) {

//		SpringApplication.run(SpringRest5Application.class, args);
		///CONTEXTO de SPRING
		/* es el lugar donde viven estos objetos de spring */
		ConfigurableApplicationContext contex = SpringApplication.run(SpringRest5Application.class, args);
	
		Coche coche = contex.getBean(Coche.class);
		System.out.println(" Iniciando spring 5 y rest UDEMY. . .");
//	 
//	 Motor motor = new Motor("Xl1",1981);
//	 Coche coche = new Coche("VW",1986,motor);

	 System.out.println("coche >> " + coche);
	}

}
