
package com.atributo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Coche {

	@Value("VW")
	private String marca;
	
	@Value("1981")
	private Integer modelo;
	
	@Autowired
	private Motor motor;
	
	
}
