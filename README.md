# Código del curso Spring Framework 5 + REST de cero a experto

# Instructor
> [Alejandro Agapito Bautista](https://www.udemy.com/course/spring-boot-rest/) 
 

#Udemy
* Spring Framework 5 + REST de cero a experto

## Contiene

* Desarrollo de Apis REST utilizando Spring framework + Spring boot
* Inversion of control
* Dependency Inyection
* Stereotypes
* Qualifiers
* Profiles
* SpEL
* Ciclo de vida de Spring beans
* Programación orientada a aspectos
* Uso de Spring boot
* Http
* Spring REST
* Spring data + JPA + H2
* Spring metrics y Actuator
* Micrometer
* Prometheus
* Grafana
* Uso de swagger
* Spring cache
* Redis
* Spring Security
* Apache Kafka

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

**cohesión de clase -> una clase debe hacer UNA sola cosa y debe hacerla bien
**Aclopamiento -> reducir la depedencie entre las clases

/************* SPRING ***********************/
 *La mejor practica es regresar el estatus para todas las respuestas REST

*Madurez REST
El “modelo de madurez definido por Richardson” y descrito por Martin Fowler ayuda a 
entender mejor los principios en los que se basa el estilo de la arquitectura RESTful.


Nivel 0: [POX]{SOAP}
         los servicios que están en este nivel utilizan HTTP como un protocolo de 
		 transporte para codificar invocaciones de servicios remotos. 
		 SOAP es un buen ejemplo de esto porque se utiliza un único endpoint para todas 
		 las operaciones.
		 
Nivel 1: [RECURSOS]{/FACTURA /insertar /borrar /update / buscar  }
         los servicios que están en este grado de madurez introducen el concepto de recurso 
		 y cada recurso tiene su propio URI que permite referenciarlo y recuperarlo 
		 directamente.
		 
Nivel 2: [VERBOS HTTP]{/ clientes (GET) /1(GET) (post) (put) (delete)}
         los servicios de este nivel usan los principales métodos del protocolo HTTP 
		 (GET, POST, PUT, DELETE).
		 
Nivel 3: [HATEOAS](Hypermedia as the Engine of Application State)
				{
				“dni”:”123456778A”,
				“nombre”:”pedro”
				"links": [         
				{   "línea": "http://dominio/facturas/1A/lineas/1" }  ,
				{   "línea": "http://dominio/facturas/1A/lineas/2" }  ,
				{   "línea": "http://dominio/facturas/1A/lineas/3" }  ,
				  ]
				}
        los servicios que están en este nivel retornan enlaces que permiten al 
		 cliente descubrir operaciones y obtener referencias a otros recursos.
		 El servicio es autodescriptivo y el cliente va navegando por los resultados 
		 para invocar las operaciones.


spring maneja inyecciones de dependencias
y la inversion de control (spring hace lo necesario para levantar el servicio)


**No mas de 50 Microservicio por Eureka-SERVER

/********************************************/
/********************* Para Angular *********/
En la carpeta donde nos encontremos correr el servidor 
C:\directorio\MockitoJunit_Basico\angular-client-books>ng serve
con:
ng -serve

 ~~~


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.1.0**  _>>>_  Iniciando proyecto [  on 10 NOV, 2021 ]  
 
 